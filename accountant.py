import sys
call = sys.argv [1:]

change_quote= 0
stock={}
logs= []
available_action= ["saldo", "sprzedaż", "zakup", "magazyn", "stop", "konto"]
balance=0

while True:
    action=input("Podaj akcję:")
    if action == "stop":
        print("Program zatrzymany")
        break
    elif action== "zakup":
        id_product = input("Podaj nazwę towaru:")
        price = int(input("Podaj cenę:"))
        qt = int(input("Podaj ilość zakupionego towaru:"))
        if price * qt > balance:
         print("Brak wystarczających środków na koncie!")
         continue
        if id_product in stock:
         stock[id_product] += qt
         print(stock)
         balance -= price * qt
         print("Saldo: {}".format(balance))
         print(stock)
         logs.append([action, id_product, price, qt])
         print(logs)
        else:
         stock[id_product] = qt
         balance -= price * qt
         print("Saldo: {}".format(balance))
         print(stock)
         logs.append([action, id_product, price, qt])
         print(logs)
    elif action=="sprzedaz":
        id_product= input("Podaj nazwę produktu:")
        price=int(input("Podaj cenę produktu:"))
        qt=int(input("Podaj ilość przedanych produktów:"))
        if qt > stock[id_product]:
            print("Brak wystarczającej ilości produktów w magazynie!")
            continue
        if id_product in stock:
            stock[id_product]-=qt
            print(stock)
            balance += price*qt
            print("Saldo:{}".format(balance))
        else:
            print("Brak produktu w magazynie!")

    elif action=="saldo":
        action=input("Wpłata czy wypłata?")
        if action=="wpłata":
            pay=int(input("Podaj wysokość wpłaty na konto:"))
            object=input("Wpłata od: ")
            balance+=pay
            print("Saldo wynosi {} po wpłacie od {}".format(balance,object))
        if action== "wypłata":
            payment=int(input("Podaj wysokośc wypłaty z konta:"))
            object=input("Przeznaczenie wypłaty:")
            if payment<balance:
                balance-=payment
                print("Saldo wynosi:{} po wypłacie na {}".format(balance, object))
            else:
             print("Brak środków na koncie!")

    elif action=="magazyn":
        print("Obecnie w magazynie masz: {}".format(stock))
    elif action=="konto":
        print("Kwota na twoim koncie to:{}".format(balance))
    else:
        print("Dostępne akcje to {}".format(",".join(available_action[:6])))